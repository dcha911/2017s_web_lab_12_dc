package ictgradschool.web.lab12.examples.example05_jooq;

import ictgradschool.web.lab12.examples.example05_jooq.generated.tables.daos.UnidbStudentsDao;
import ictgradschool.web.lab12.examples.example05_jooq.generated.tables.pojos.UnidbStudents;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import static ictgradschool.web.lab12.examples.example05_jooq.generated.Tables.UNIDB_STUDENTS;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public class Example05c {

    public static void main(String[] args) throws SQLException, IOException {

        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("connection.properties")) {
            dbProps.load(fis);
        }

        // Establishing connection to the database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            // Access the JOOQ library through this variable.
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

            // Get all students (as a List of UnidbStudents objects).
            // NOTE: MAKE SURE you are importing the correct UnidbStudents object - there's more than one!!
            // Import the one in the *.pojos package (see import statements above).
            List<UnidbStudents> students =
                    create.select()
                    .from(UNIDB_STUDENTS)
                    .fetch()
                    .into(UnidbStudents.class);

            System.out.println("All students:");
            for (UnidbStudents student : students) {
                System.out.println("- " + student);
            }
            System.out.println();


            // Get all students from NZ, using the Students DAO.
            UnidbStudentsDao dao = new UnidbStudentsDao(create.configuration());

            List<UnidbStudents> studentsFromNZ = dao.fetchByCountry("NZ");

            System.out.println("Students from NZ:");
            for (UnidbStudents student : studentsFromNZ) {
                System.out.println("- " + student);
            }

        }

    }

}
