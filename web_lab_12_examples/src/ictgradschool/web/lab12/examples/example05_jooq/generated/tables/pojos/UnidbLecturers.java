/*
 * This file is generated by jOOQ.
*/
package ictgradschool.web.lab12.examples.example05_jooq.generated.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UnidbLecturers implements Serializable {

    private static final long serialVersionUID = 1066282535;

    private Integer staffNo;
    private String  fname;
    private String  lname;
    private String  office;

    public UnidbLecturers() {}

    public UnidbLecturers(UnidbLecturers value) {
        this.staffNo = value.staffNo;
        this.fname = value.fname;
        this.lname = value.lname;
        this.office = value.office;
    }

    public UnidbLecturers(
        Integer staffNo,
        String  fname,
        String  lname,
        String  office
    ) {
        this.staffNo = staffNo;
        this.fname = fname;
        this.lname = lname;
        this.office = office;
    }

    public Integer getStaffNo() {
        return this.staffNo;
    }

    public void setStaffNo(Integer staffNo) {
        this.staffNo = staffNo;
    }

    public String getFname() {
        return this.fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return this.lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getOffice() {
        return this.office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("UnidbLecturers (");

        sb.append(staffNo);
        sb.append(", ").append(fname);
        sb.append(", ").append(lname);
        sb.append(", ").append(office);

        sb.append(")");
        return sb.toString();
    }
}
