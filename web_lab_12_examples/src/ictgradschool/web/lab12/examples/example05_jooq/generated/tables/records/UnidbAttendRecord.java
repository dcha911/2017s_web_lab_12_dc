/*
 * This file is generated by jOOQ.
*/
package ictgradschool.web.lab12.examples.example05_jooq.generated.tables.records;


import ictgradschool.web.lab12.examples.example05_jooq.generated.tables.UnidbAttend;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record3;
import org.jooq.Record5;
import org.jooq.Row5;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UnidbAttendRecord extends UpdatableRecordImpl<UnidbAttendRecord> implements Record5<Integer, String, String, String, String> {

    private static final long serialVersionUID = -13633147;

    /**
     * Setter for <code>uni-db.unidb_attend.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>uni-db.unidb_attend.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>uni-db.unidb_attend.dept</code>.
     */
    public void setDept(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>uni-db.unidb_attend.dept</code>.
     */
    public String getDept() {
        return (String) get(1);
    }

    /**
     * Setter for <code>uni-db.unidb_attend.num</code>.
     */
    public void setNum(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>uni-db.unidb_attend.num</code>.
     */
    public String getNum() {
        return (String) get(2);
    }

    /**
     * Setter for <code>uni-db.unidb_attend.semester</code>.
     */
    public void setSemester(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>uni-db.unidb_attend.semester</code>.
     */
    public String getSemester() {
        return (String) get(3);
    }

    /**
     * Setter for <code>uni-db.unidb_attend.mark</code>.
     */
    public void setMark(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>uni-db.unidb_attend.mark</code>.
     */
    public String getMark() {
        return (String) get(4);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record3<Integer, String, String> key() {
        return (Record3) super.key();
    }

    // -------------------------------------------------------------------------
    // Record5 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row5<Integer, String, String, String, String> fieldsRow() {
        return (Row5) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row5<Integer, String, String, String, String> valuesRow() {
        return (Row5) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return UnidbAttend.UNIDB_ATTEND.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return UnidbAttend.UNIDB_ATTEND.DEPT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return UnidbAttend.UNIDB_ATTEND.NUM;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return UnidbAttend.UNIDB_ATTEND.SEMESTER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field5() {
        return UnidbAttend.UNIDB_ATTEND.MARK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getDept();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getNum();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getSemester();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component5() {
        return getMark();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getDept();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getNum();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getSemester();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value5() {
        return getMark();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnidbAttendRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnidbAttendRecord value2(String value) {
        setDept(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnidbAttendRecord value3(String value) {
        setNum(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnidbAttendRecord value4(String value) {
        setSemester(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnidbAttendRecord value5(String value) {
        setMark(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnidbAttendRecord values(Integer value1, String value2, String value3, String value4, String value5) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached UnidbAttendRecord
     */
    public UnidbAttendRecord() {
        super(UnidbAttend.UNIDB_ATTEND);
    }

    /**
     * Create a detached, initialised UnidbAttendRecord
     */
    public UnidbAttendRecord(Integer id, String dept, String num, String semester, String mark) {
        super(UnidbAttend.UNIDB_ATTEND);

        set(0, id);
        set(1, dept);
        set(2, num);
        set(3, semester);
        set(4, mark);
    }
}
