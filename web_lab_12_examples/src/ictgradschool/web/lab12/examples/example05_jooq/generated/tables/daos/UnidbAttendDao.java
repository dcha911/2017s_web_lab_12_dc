/*
 * This file is generated by jOOQ.
*/
package ictgradschool.web.lab12.examples.example05_jooq.generated.tables.daos;


import ictgradschool.web.lab12.examples.example05_jooq.generated.tables.UnidbAttend;
import ictgradschool.web.lab12.examples.example05_jooq.generated.tables.records.UnidbAttendRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.Record3;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UnidbAttendDao extends DAOImpl<UnidbAttendRecord, ictgradschool.web.lab12.examples.example05_jooq.generated.tables.pojos.UnidbAttend, Record3<Integer, String, String>> {

    /**
     * Create a new UnidbAttendDao without any configuration
     */
    public UnidbAttendDao() {
        super(UnidbAttend.UNIDB_ATTEND, ictgradschool.web.lab12.examples.example05_jooq.generated.tables.pojos.UnidbAttend.class);
    }

    /**
     * Create a new UnidbAttendDao with an attached configuration
     */
    public UnidbAttendDao(Configuration configuration) {
        super(UnidbAttend.UNIDB_ATTEND, ictgradschool.web.lab12.examples.example05_jooq.generated.tables.pojos.UnidbAttend.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Record3<Integer, String, String> getId(ictgradschool.web.lab12.examples.example05_jooq.generated.tables.pojos.UnidbAttend object) {
        return compositeKeyRecord(object.getId(), object.getDept(), object.getNum());
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<ictgradschool.web.lab12.examples.example05_jooq.generated.tables.pojos.UnidbAttend> fetchById(Integer... values) {
        return fetch(UnidbAttend.UNIDB_ATTEND.ID, values);
    }

    /**
     * Fetch records that have <code>dept IN (values)</code>
     */
    public List<ictgradschool.web.lab12.examples.example05_jooq.generated.tables.pojos.UnidbAttend> fetchByDept(String... values) {
        return fetch(UnidbAttend.UNIDB_ATTEND.DEPT, values);
    }

    /**
     * Fetch records that have <code>num IN (values)</code>
     */
    public List<ictgradschool.web.lab12.examples.example05_jooq.generated.tables.pojos.UnidbAttend> fetchByNum(String... values) {
        return fetch(UnidbAttend.UNIDB_ATTEND.NUM, values);
    }

    /**
     * Fetch records that have <code>semester IN (values)</code>
     */
    public List<ictgradschool.web.lab12.examples.example05_jooq.generated.tables.pojos.UnidbAttend> fetchBySemester(String... values) {
        return fetch(UnidbAttend.UNIDB_ATTEND.SEMESTER, values);
    }

    /**
     * Fetch records that have <code>mark IN (values)</code>
     */
    public List<ictgradschool.web.lab12.examples.example05_jooq.generated.tables.pojos.UnidbAttend> fetchByMark(String... values) {
        return fetch(UnidbAttend.UNIDB_ATTEND.MARK, values);
    }
}
