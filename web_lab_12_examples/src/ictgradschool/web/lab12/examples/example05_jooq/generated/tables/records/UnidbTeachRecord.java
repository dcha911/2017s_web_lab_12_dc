/*
 * This file is generated by jOOQ.
*/
package ictgradschool.web.lab12.examples.example05_jooq.generated.tables.records;


import ictgradschool.web.lab12.examples.example05_jooq.generated.tables.UnidbTeach;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UnidbTeachRecord extends UpdatableRecordImpl<UnidbTeachRecord> implements Record3<String, String, Integer> {

    private static final long serialVersionUID = 1921902588;

    /**
     * Setter for <code>uni-db.unidb_teach.dept</code>.
     */
    public void setDept(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>uni-db.unidb_teach.dept</code>.
     */
    public String getDept() {
        return (String) get(0);
    }

    /**
     * Setter for <code>uni-db.unidb_teach.num</code>.
     */
    public void setNum(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>uni-db.unidb_teach.num</code>.
     */
    public String getNum() {
        return (String) get(1);
    }

    /**
     * Setter for <code>uni-db.unidb_teach.staff_no</code>.
     */
    public void setStaffNo(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>uni-db.unidb_teach.staff_no</code>.
     */
    public Integer getStaffNo() {
        return (Integer) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record3<String, String, Integer> key() {
        return (Record3) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<String, String, Integer> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<String, String, Integer> valuesRow() {
        return (Row3) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field1() {
        return UnidbTeach.UNIDB_TEACH.DEPT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return UnidbTeach.UNIDB_TEACH.NUM;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field3() {
        return UnidbTeach.UNIDB_TEACH.STAFF_NO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component1() {
        return getDept();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getNum();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component3() {
        return getStaffNo();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value1() {
        return getDept();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getNum();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value3() {
        return getStaffNo();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnidbTeachRecord value1(String value) {
        setDept(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnidbTeachRecord value2(String value) {
        setNum(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnidbTeachRecord value3(Integer value) {
        setStaffNo(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnidbTeachRecord values(String value1, String value2, Integer value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached UnidbTeachRecord
     */
    public UnidbTeachRecord() {
        super(UnidbTeach.UNIDB_TEACH);
    }

    /**
     * Create a detached, initialised UnidbTeachRecord
     */
    public UnidbTeachRecord(String dept, String num, Integer staffNo) {
        super(UnidbTeach.UNIDB_TEACH);

        set(0, dept);
        set(1, num);
        set(2, staffNo);
    }
}
