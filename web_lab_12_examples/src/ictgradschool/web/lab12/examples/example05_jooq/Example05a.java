package ictgradschool.web.lab12.examples.example05_jooq;

import org.jooq.*;
import org.jooq.impl.DSL;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static ictgradschool.web.lab12.examples.example05_jooq.generated.Tables.*;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public class Example05a {

    public static void main(String[] args) throws SQLException, IOException {

        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("connection.properties")) {
            dbProps.load(fis);
        }

        // Establishing connection to the database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            // Access the JOOQ library through this variable.
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

            // Get all info about all lecturers
            Result<Record> allLecturersResult = create.select().from(UNIDB_LECTURERS).fetch();

            // Print them out.
            System.out.println("Lecturers:");
            for (Record record : allLecturersResult) {
                String name = record.getValue(UNIDB_LECTURERS.FNAME) + " " + record.getValue(UNIDB_LECTURERS.LNAME);
                System.out.println("- " + name);
            }
            System.out.println();

            // Get the first & last names of all students from NZ
            Result<Record2<String, String>> studentsFromNZResult =
                    create.select(UNIDB_STUDENTS.FNAME, UNIDB_STUDENTS.LNAME)
                    .from(UNIDB_STUDENTS)
                    .where(UNIDB_STUDENTS.COUNTRY.eq("NZ")).fetch();

            // Print them out.
            System.out.println("Students from NZ:");
            for (Record record : studentsFromNZResult) {
                String name = record.getValue(UNIDB_STUDENTS.FNAME) + " " + record.getValue(UNIDB_STUDENTS.LNAME);
                System.out.println("- " + name);
            }
            System.out.println();

            // Get the course department and number of all courses taught by Te Taka
            Result<Record2<String, String>> coursesByTeTakaResult =
                    create.select(UNIDB_TEACH.DEPT, UNIDB_TEACH.NUM)
                    .from(UNIDB_TEACH)
                    .join(UNIDB_LECTURERS)
                    .on(UNIDB_TEACH.STAFF_NO.eq(UNIDB_LECTURERS.STAFF_NO))
                    .where(UNIDB_LECTURERS.FNAME.eq("Te Taka")).fetch();

            // Print them out.
            System.out.println("Courses by Te Taka:");
            for (Record record : coursesByTeTakaResult) {
                String name = record.getValue(UNIDB_TEACH.DEPT) + " " + record.getValue(UNIDB_TEACH.NUM);
                System.out.println("- " + name);
            }
            System.out.println();
        }

    }

}
