package ictgradschool.web.lab12.examples.example05_jooq;

import org.jooq.*;
import org.jooq.impl.DSL;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static ictgradschool.web.lab12.examples.example05_jooq.generated.Tables.*;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public class Example05b {

    public static void main(String[] args) throws SQLException, IOException {

        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("connection.properties")) {
            dbProps.load(fis);
        }

        // Establishing connection to the database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            // Access the JOOQ library through this variable.
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

            // Insert some stuff
            int numRows = create.insertInto(UNIDB_LECTURERS).values(12345, "Thomas", "T-Rex", "L.5.87").execute();
            System.out.println(numRows + " rows inserted.");

            // Insert some more stuff
            numRows = create.insertInto(UNIDB_LECTURERS)
                    .values(90211, "Tarn", "Adams", "Place1")
                    .values(90212, "Jay", "Bates", "Place2")
                    .values(90213, "Tim", "Cook", "Place3")
                    .values(90214, "Bill", "Gates", "Place4")
                    .execute();
            System.out.println(numRows + " rows inserted.");

            // Delete some stuff
            numRows = create.deleteFrom(UNIDB_LECTURERS)
                    .where(UNIDB_LECTURERS.STAFF_NO.gt(10000))
                    .execute();
            System.out.println(numRows + " rows deleted.");

        }

    }

}
