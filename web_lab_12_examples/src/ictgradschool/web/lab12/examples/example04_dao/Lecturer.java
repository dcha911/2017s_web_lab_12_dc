package ictgradschool.web.lab12.examples.example04_dao;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public class Lecturer {

    private Integer staffNo;

    private String firstName;

    private String lastName;

    private String office;

    public Lecturer() {}

    public Lecturer(int staffNo, String firstName, String lastName, String office) {
        this.staffNo = staffNo;
        this.firstName = firstName;
        this.lastName = lastName;
        this.office = office;
    }

    public Integer getStaffNo() {
        return staffNo;
    }

    public void setStaffNo(Integer staffNo) {
        this.staffNo = staffNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    @Override
    public String toString() {
        return "Lecturer{" +
                "staffNo=" + staffNo +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", office='" + office + '\'' +
                '}';
    }
}
