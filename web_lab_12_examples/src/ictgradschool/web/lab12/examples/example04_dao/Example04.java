package ictgradschool.web.lab12.examples.example04_dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public class Example04 {

    public static void main(String[] args) throws IOException, SQLException {

        try (UniDAO dao = new UniDAO(new MySQLDatabase())) {

            Lecturer thomas = new Lecturer(12345, "Thomas", "T-Rex", "L.5.87");
            dao.addLecturer(thomas);
            System.out.println("Added Thomas to db.");

            System.out.println();

            System.out.println("All lecturers: ");
            List<Lecturer> lecturers = dao.getAllLecturers();
            for (Lecturer l : lecturers) {

                System.out.println(l);

            }

            System.out.println();

            Lecturer thomasFromDB = dao.getLecturerById(12345);
            System.out.println("Thomas:");
            System.out.println(thomasFromDB);

            System.out.println();

            dao.deleteLecturer(12345);
            System.out.println("Deleted Thomas from db.");

            System.out.println();

            thomasFromDB = dao.getLecturerById(12345);
            System.out.println("Thomas (should be null):");
            System.out.println(thomasFromDB);

        }

    }

}
