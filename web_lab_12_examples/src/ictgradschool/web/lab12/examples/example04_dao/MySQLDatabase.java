package ictgradschool.web.lab12.examples.example04_dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public class MySQLDatabase implements Database {

    @Override
    public Connection getConnection() throws IOException, SQLException {

        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("connection.properties")) {
            dbProps.load(fis);
        }

        Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps);
        return conn;
    }
}
