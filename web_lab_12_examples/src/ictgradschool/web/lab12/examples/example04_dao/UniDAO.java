package ictgradschool.web.lab12.examples.example04_dao;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public class UniDAO implements AutoCloseable {

    private final Database db;
    private final Connection conn;

    /**
     * Creates a new UniDAO and establishes a connection to the given database.
     */
    public UniDAO(Database db) throws IOException, SQLException {
        this.db = db;
        this.conn = db.getConnection();
    }

    /**
     * Executes a query to get all lecturers from the db, then converts each row of the result table
     * to a Lecturer object and returns them all in a list.
     */
    public List<Lecturer> getAllLecturers() throws SQLException {

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM unidb_lecturers")) {
            try (ResultSet rs = stmt.executeQuery()) {

                List<Lecturer> lecturers = new ArrayList<>();
                while (rs.next()) {
                    lecturers.add(lecturerFromResultSet(rs));
                }

                return lecturers;

            }
        }

    }

    /**
     * Executes a query to get the lecturer with the given id from the db, then converts the result
     * to a Lecturer object and returns it. Returns null if the lecturer couldn't be found.
     */
    public Lecturer getLecturerById(int staffNo) throws SQLException {

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM unidb_lecturers WHERE staff_no = ?;")) {

            stmt.setInt(1, staffNo);

            try (ResultSet rs = stmt.executeQuery()) {

                if (rs.next()) {
                    return lecturerFromResultSet(rs);
                } else {
                    return null;
                }

            }
        }

    }

    /**
     * Adds the given lecturer to the database.
     */
    public void addLecturer(Lecturer lecturer) throws SQLException {

        try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO unidb_lecturers VALUES (?, ?, ?, ?);")) {

            stmt.setInt(1, lecturer.getStaffNo());
            stmt.setString(2, lecturer.getFirstName());
            stmt.setString(3, lecturer.getLastName());
            stmt.setString(4, lecturer.getOffice());

            stmt.executeUpdate();

        }

    }

    /**
     * Modifies the given existing lecturer in the database.
     */
    public void updateLecturer(Lecturer lecturer) throws SQLException {

        try (PreparedStatement stmt = conn.prepareStatement("UPDATE unidb_lecturers SET fname = ?, lname = ?, office = ? WHERE staff_no = ?;")) {

            stmt.setString(1, lecturer.getFirstName());
            stmt.setString(2, lecturer.getLastName());
            stmt.setString(3, lecturer.getOffice());
            stmt.setInt(4, lecturer.getStaffNo());

            stmt.executeUpdate();

        }

    }

    /**
     * Deletes the lecturer with the given id from the database.
     */
    public void deleteLecturer(int staffNo) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM unidb_lecturers WHERE staff_no = ?;")) {

            stmt.setInt(1, staffNo);
            stmt.executeUpdate();

        }
    }

    /**
     * Translates the current row of the given ResultSet into a Lecturer object.
     */
    private Lecturer lecturerFromResultSet(ResultSet rs) throws SQLException {
        return new Lecturer(rs.getInt(1), rs.getString(2),
                rs.getString(3), rs.getString(4));
    }

    /**
     * Closes the connection to the database. Implements {@link AutoCloseable} to support try-with-resources.
     */
    @Override
    public void close() throws SQLException {
        this.conn.close();
    }
}
