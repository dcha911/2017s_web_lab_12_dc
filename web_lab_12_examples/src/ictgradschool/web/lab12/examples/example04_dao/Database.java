package ictgradschool.web.lab12.examples.example04_dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public interface Database {

    Connection getConnection() throws IOException, SQLException;

}
