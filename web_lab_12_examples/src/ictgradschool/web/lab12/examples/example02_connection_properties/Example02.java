package ictgradschool.web.lab12.examples.example02_connection_properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public class Example02 {

    public static void main(String[] args) throws SQLException, IOException {

        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("connection.properties")) {
            dbProps.load(fis);
        }

        // Establishing connection to the database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            // you use the connection here


            // A statement with no results
            try (Statement stmt = conn.createStatement()) {
                int rowsAffected = stmt.executeUpdate("INSERT INTO unidb_lecturers VALUES (12345, 'Thomas', 'T-Rex', 'L.5.87');");
                System.out.println("Inserted " + rowsAffected + " rows.");

            }
            // Note the use of try-with-resources here again.

            // Another statement with no results
            try (Statement stmt = conn.createStatement()) {
                int rowsAffected = stmt.executeUpdate("DELETE FROM unidb_lecturers WHERE staff_no = 12345");
                System.out.println("Deleted " + rowsAffected + " rows.");

            }

            // A statement that returns some results
            try (Statement stmt = conn.createStatement()) {
                try (ResultSet r = stmt.executeQuery("SELECT * FROM unidb_lecturers;")) {

                    // A result set represents a table of results returned from an SQL query.
                    // Can loop through each row like this:
                    while (r.next()) { // r.next() moves the db cursor to the next line, and returns false if we're finished looping.

                        // Within this while-loop, all the r.get*** methods will return values from columns on the current row.

                        // Can get values by column name like this.
                        int staff_no = r.getInt("staff_no");
                        String fname = r.getString("fname");
                        System.out.println("Staff number: " + staff_no + ", first name: " + fname);

                        // Can also get values by column index. However, note that the index is 1-based.
                        // So in this case, column "1" is the staff_no column.
                        int alsoStaffNo = r.getInt(1);
                        System.out.println("Staff number again: " + alsoStaffNo);


                    }
                    // Note that this code is NOT skipping the first line! The ResultSet starts with its cursor
                    // pointing to just BEFORE the first row.

                }
            }


        } // with 'try-resource' the VM will take care of closing the connection


    }

}
