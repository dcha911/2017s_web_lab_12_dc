package ictgradschool.web.lab12.examples.example03_prepared_statement;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public class Example03 {

    public static void main(String[] args) throws SQLException, IOException {

        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("connection.properties")) {
            dbProps.load(fis);
        }

        // Establishing connection to the database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            // Statements which return no results
            try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO unidb_lecturers VALUES (?, ?, ?, ?);")) {

                stmt.setInt(1, 12345);
                stmt.setString(2, "Thomas");
                stmt.setString(3, "T-Rex");
                stmt.setString(4, "L.5.87");

                int rowsAffected = stmt.executeUpdate();
                System.out.println("Inserted " + rowsAffected + " rows.");

            }

            // A statement that returns some results
            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM unidb_lecturers WHERE staff_no > ?")) {

                stmt.setInt(1, 500);

                try (ResultSet r = stmt.executeQuery()) {

                    while (r.next()) {

                        int staff_no = r.getInt("staff_no");
                        String fname = r.getString("fname");
                        System.out.println("Staff number: " + staff_no + ", first name: " + fname);
                    }
                }
            }

            // One more statement
            try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM unidb_lecturers WHERE staff_no = ?;")) {

                stmt.setInt(1, 12345);

                int rowsAffected = stmt.executeUpdate();
                System.out.println("Deleted " + rowsAffected + " rows.");

            }

        }
    }
}
