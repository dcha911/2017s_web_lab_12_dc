package ictgradschool.web.lab12.ex1;

import ictgradschool.web.lab12.Keyboard;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.Key;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {


    public static void main(String[] args) {

        while (true) {

            System.out.println("enter a partial article");
            String s = Keyboard.readInput();

        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
            Properties dbProps = new Properties();

            try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
                dbProps.load(fIn);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Set the database name to your database
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
                System.out.println("Connection successful");

                try (Statement stmt = conn.createStatement()) {
                    try (ResultSet r = stmt.executeQuery("SELECT body FROM simpledao_articles WHERE simpledao_articles.title LIKE \"" + s + "%\"")) {
                        while (r.next()) {
                            String a = r.getString(1);
                            System.out.println(a);
                        }

                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
