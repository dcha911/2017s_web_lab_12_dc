package ictgradschool.web.lab12.ex2;

import ictgradschool.web.lab12.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by dcha911 on 4/01/2018.
 */
public class ArticleDAO {


    public static void allArticles(String s) {

            List<Article> articles = new ArrayList<>();

            Properties dbProps = new Properties();

            try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
                dbProps.load(fIn);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Set the database name to your database
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
                System.out.println("Connection successful");

                try (Statement stmt = conn.createStatement()) {
                    try (ResultSet r = stmt.executeQuery("SELECT body FROM simpledao_articles WHERE simpledao_articles.title LIKE \"" + s + "%\"")) {
                        while (r.next()) {
                            Article a = new Article();
                            a.setBody(r.getString(1));
                            articles.add(a);
                        }

                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            for (int i = 0; i <articles.size() ; i++) {
                System.out.println(articles.get(i).getBody());
            }
        }
    }
