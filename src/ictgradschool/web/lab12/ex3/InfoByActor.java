package ictgradschool.web.lab12.ex3;

import ictgradschool.web.lab12.Keyboard;
import ictgradschool.web.lab12.ex3.generated.tables.PfilmsActor;
import org.jooq.*;
import org.jooq.impl.DSL;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import static ictgradschool.web.lab12.ex3.generated.Tables.*;
import static org.jooq.impl.DSL.table;

/**
 * Created by dcha911 on 4/01/2018.
 */
public class InfoByActor {

    public static void start () {

        System.out.println("Please enter the name of the actor you wish to\n" +
                "get information about, or press enter to return\n" +
                "to the previous menu");

        String s = Keyboard.readInput();

        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
//            System.out.println(conn.isClosed());
            System.out.println("Connection successful");
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);
            Result <Record4<String,String,String,String>> r = create.select(PFILMS_ACTOR.ACTOR_FNAME,PFILMS_ACTOR.ACTOR_LNAME,PFILMS_FILM.FILM_TITLE,PFILMS_ROLE.ROLE_NAME).from(PFILMS_PARTICIPATES_IN,PFILMS_ACTOR,PFILMS_FILM,PFILMS_ROLE).where(PFILMS_ACTOR.ACTOR_FNAME.eq(s)).and(PFILMS_ACTOR.ACTOR_ID.eq(PFILMS_PARTICIPATES_IN.ACTOR_ID)).and(PFILMS_PARTICIPATES_IN.FILM_ID.eq(PFILMS_FILM.FILM_ID)).and(PFILMS_PARTICIPATES_IN.ROLE_ID.eq(PFILMS_ROLE.ROLE_ID)).fetch();
//            System.out.println(r);

            String str = r.get(0).getValue(PFILMS_ACTOR.ACTOR_FNAME) +" " + r.get(0).getValue(PFILMS_ACTOR.ACTOR_LNAME);
            System.out.println(str + " is listed as being involved in the following films: \n");
            for (Record rs : r) {
                String name = rs.getValue(PFILMS_FILM.FILM_TITLE) + " " + rs.getValue(PFILMS_ROLE.ROLE_NAME);
                System.out.println(name);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
