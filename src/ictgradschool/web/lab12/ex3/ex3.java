package ictgradschool.web.lab12.ex3;

import ictgradschool.web.lab12.Keyboard;

/**
 * Created by dcha911 on 4/01/2018.
 */
public class ex3 {

    private void start() {

        greeting();

        outerLoop:
        while (true) {
            String choice = Keyboard.readInput();

            switch (choice) {
                case "1":
                    InfoByActor.start();
                    break;
                case "2":
                    InfoByMovie.start();
                    break;
                case "3":
                    InfoByGenre.start();
                    break;
                case "4":
                    break outerLoop;
                default:
                    System.out.println("that's not a valid choice");
                    greeting();
            }
        }
    }

    public void greeting() {
        System.out.println("Welcome to the Film database!");
        System.out.println("Please select an option from the following");
        System.out.println("1. Information by Actor");
        System.out.println("2. Information by Movie");
        System.out.println("3. Information by Genre");
        System.out.println("4. Exit");
    }

    public static void main(String[] args) {

        ex3 ex3 = new ex3();
        ex3.start();
    }

}
